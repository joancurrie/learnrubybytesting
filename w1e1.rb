
y = false      #an assignment
z = true       #an assignment
x = y or z     # = has highest precedence or, therefore x = y is perform before or
               # x = y
               # x assigned false (because y = false)
               #            z = true
               #
puts x         #Therefore x is false.
#-----------------------------------
(x = y) or z   # (x = y)
               # Therefore x = false
               #                     z = true
               #           x or z
               #       false or true
               #
puts x         # Therefore x is false.
#------------------------------------
x = (y or z)   # x = (false or true)
               # or is always unless both y and z are false.
puts x         # x is true.
