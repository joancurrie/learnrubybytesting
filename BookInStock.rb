class BookInStock
  #ACCESSOR methods
  attr_reader :isbn
  attr_accessor :price

  #provide object with initialize method.
  def initialize(isbn, price)
    @isbn = isbn
    @price = Float(price)
  end

  #ACCESSOR methods:
  #def isbn
  #  @isbn
  #end
  #def price
  #  @price
  #end

  def price_in_cent
    Integer(price*100 + 0.5)
  end
  def price=(new_price)
    @price = new_price
  end

#  b1 = BookInStock.new("isbn1", 3)
#  puts b1
#  b2 = BookInStock.new("isbn2", 3.14)
#  puts b2
#  b3 = BookInStock.new("isbn3", "5.67")
#  puts b3
end

book = BookInStock.new( "isbn1", 33.80)
puts "ISBN = #{book.isbn}"
puts  "Price = #{book.price}"
buts  "Price in cent = #{book.price_in_cent}"
book.price = book.price*0.75
puts  "New price = #{book.price}"
